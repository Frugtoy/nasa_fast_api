from datetime import datetime
from pydantic import BaseModel, HttpUrl, validator, ValidationError
from typing import List ,Optional
import re

class Creeds(BaseModel):
    api_key: str
    method_name: str


class RequestBody(BaseModel):
    id: str


class Sources(BaseModel):
    sources: str


class Request(BaseModel):
    requestBody: RequestBody
    sources: Sources


class Input(BaseModel):
    request_item_id: int
    source_code: str
    credentials: HttpUrl
    callback_url: str
    requests: list[Request]

    @validator('credentials')
    def url_validation(cls, url_):
        if 'https://api.nasa.gov/neo/rest/v1/feed' not in url_:
            raise ValueError("I can only work with https://api.nasa.gov/neo/rest/v1/feed api method ")
            return _url
        query_params = ['start_date', 'end_date', 'api_key']
        for param in query_params:
            if param not in url_:
                raise_msg = "there is no " + param + " query params"
                raise ValueError(raise_msg)
                break
        return url_



class Error(BaseModel):
    code: int = 0
    message: str = ''


class ExternalSourceLog(BaseModel):
    call_time: datetime = None
    call_duration: float = 0.0
    http_code: int = -1
    method_name: str = 'no'



class Output(BaseModel):
    request_item_id: int = 0
    response: str = ''
    cache: str = ''
    error: Optional[Error]
    external_source_logs: List[ExternalSourceLog] = []

    def dict(self):
        if self.error is not None:
            dictionary = \
                {
                    "request_item_id": self.request_item_id,
                    "response": self.response,
                    "error":
                        {
                            "code": self.error.code,
                            "message": self.error.message
                        },
                    "cache": self.cache,
                    "external_source_logs":  [i.dict() for i in self.external_source_logs]
                }
        elif self.error is None:
            dictionary = \
                {
                    "request_item_id": self.request_item_id,
                    "response": self.response,
                    "cache": self.cache,
                    "external_source_logs":  [i.dict() for i in self.external_source_logs]

                }
        return dictionary

#class Response_model:
#    out: Output
#    External_source_logs: List[External_source_log]

'''
{
"request_item_id": число,
"source_code": строка,
"credentials": Строка с урлом и ключом апи в объекте,
"callback_url": строка-ссылка, куда должен прилететь ответ в итоге
"requests": [
{
"request_body": {
"id": строка
},
"sources": "Строка
}
}
]


На выходе у тебя должен быть объект такой

{
"request_item_id": число,
"response": Строка, в которой json, содержащий все объекты ответы методов,
"cache": Строка, в которой json, содержащий число астероидов, массив их имен, среднее и сумма,
"error": {
"code": число,
"message": строка
}
}

И ещё в ответе
"external_sources_logs": массив, в котором каждый объект – это
{
"call_time": датавремя,
"call_duration": длительность до получения ответа в мс,
"http_code": код ответа сервера,
"method_name": Какой метод ты сейчас вызываешь
}


'''