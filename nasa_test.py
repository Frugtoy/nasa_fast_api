from fastapi import FastAPI
from schemas import *
import requests as req
import uvicorn

test_app = FastAPI(title="TEST_SERVICE")


@test_app.get("/health")
async def health():
    return {"message": "OK 200"}


@test_app.post("/request")
async def requ(input_request:Input):
    result = 'OK'
    try:
        result = req.post("http://127.0.0.1:8000/getData",data =  input_request.json())
        print(result.json())
    except Exception as e:
        print(e)
    finally:
        return {"message": "ended"}


@test_app.post("/callBack")
async def callBack(output_data: Output):
    print("CALLBACK", output_data.cache)
    return {"message": "OK 200"}

if __name__ == "__main__":
    uvicorn.run(test_app, host="0.0.0.0", port=4001)