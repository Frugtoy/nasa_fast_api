from fastapi import FastAPI
from schemas import *
import pprint
import requests as req
import re
import threading
import time
from datetime import datetime
import json
import uvicorn
app = FastAPI(title="NASA_SERVICE")


class MyEncoder(json.JSONEncoder):
    def default(self, object_):
        if isinstance(object_, set):
            return list(object_)
        return object


class RequestWorker:

    def __init__(self, _input: Input):
        self.input_req = _input
        self.output_req = Output()
        self.output_req.request_item_id = self.input_req.request_item_id
        self.api_key = get_api_key(_input.credentials)

    def set_lookup_url(self, _id):
        url = "https://api.nasa.gov/neo/rest/v1/neo/%s?api_key=" % (str(_id)) + self.api_key
        return url

    def serialise_json(self):
        print("SERIALISATION")
        json_string = json.dumps(self.output_req.dict(), cls=MyEncoder, allow_nan=True)
        #with open('out.json', 'w') as f:
        #    f.write(json_string)
        return json_string


@app.get("/")
async def root():
    return {"message": "OK 200"}


@app.get("/health")
async def health():
    return {"message": "OK 200"}


@app.post("/getData")
async def get_data(input_request: Input):
    print("request into" + input_request.callback_url + " callback url")
    t = threading.Thread(target=send_feed_request, args=(input_request,))
    t.start()
    return {"message": "OK 200"}


'''https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key=sFoV70fGmKody5IAJ0A8qhYBkX37zmhUhGZkjWUO'''
'''https://api.nasa.gov/neo/rest/v1/neo/3542519?api_key=DEMO_KEY'''


def send_feed_request(input_request: Input):
    req_worker = RequestWorker(input_request)
    #pprint.pprint(input_request.dict())
    try:
        req_worker.output_req.response = ''
        feed_external_log = ExternalSourceLog()
        feed_external_log.call_time = get_call_time()
        time_before_request = time.time()
        ans = req.get(input_request.credentials)
        req_worker.output_req.response = req_worker.output_req.response + json.dumps(ans.json())
        feed_external_log.call_duration = time.time() - time_before_request
        feed_external_log.method_name = "feed"
        feed_external_log.http_code = int(ans.status_code)
        req_worker.output_req.external_source_logs.append(feed_external_log)
        if ans.status_code == 200:
            all_asteroids = []
            calculated_asteroids = []
            number_to_calculate = 5  # You can change it on any value you'd like to
            average_kilometers_estimated_diameter_max = 0.0
            sum_kilometers_estimated_diameter_min = 0.0
            for asteroids in ans.json()["near_earth_objects"].keys():
                for current_asteroid in ans.json()["near_earth_objects"][asteroids]:
                    all_asteroids.append(current_asteroid['id'])
            list_size = len(all_asteroids)
            number_to_calculate = number_to_calculate if list_size >= number_to_calculate else list_size

            for i in range(number_to_calculate):
                print(all_asteroids[i])
                calculated_asteroids.append(all_asteroids[i])
                current_external_source_log = ExternalSourceLog()
                current_external_source_log.call_time = get_call_time()
                start_req = time.time()
                ans = req.get(req_worker.set_lookup_url(all_asteroids[i]))
                current_external_source_log.method_name = "lookup"
                current_external_source_log.http_code = ans.status_code
                current_external_source_log.call_duration = (time.time() - start_req) * 1000
                req_worker.output_req.external_source_logs.append(current_external_source_log)

                if ans.status_code == 200:
                    req_worker.output_req.response = req_worker.output_req.response + json.dumps(ans.json())
                    sum_kilometers_estimated_diameter_min = sum_kilometers_estimated_diameter_min + \
                                                            ans.json()["estimated_diameter"]["kilometers"]["estimated_diameter_min"]
                    average_kilometers_estimated_diameter_max = average_kilometers_estimated_diameter_max + \
                                                            ans.json()["estimated_diameter"]["kilometers"]["estimated_diameter_max"]
                    sum_kilometers_estimated_diameter_min = sum_kilometers_estimated_diameter_min + \
                                                            ans.json()["estimated_diameter"]["kilometers"]["estimated_diameter_min"]

                else:
                    req_worker.output_req.error = Error()
                    print(ans.content)
                    average_kilometers_estimated_diameter_max = -1
                    sum_kilometers_estimated_diameter_min = -1
                    req_worker.output_req.error.code = ans.status_code
                    req_worker.output_req.error.message = ans.json()["error_message"]
                    break

            if req_worker.output_req.error is None:
                average_kilometers_estimated_diameter_max = average_kilometers_estimated_diameter_max / number_to_calculate
                print(average_kilometers_estimated_diameter_max, sum_kilometers_estimated_diameter_min)
                raw_cache_dict = {
                    "number_of_asteroids": number_to_calculate,
                    "asteroid_name": calculated_asteroids,
                    "average_kilometers_estimated_diameter_max": average_kilometers_estimated_diameter_max,
                    "sum_kilometers_estimated_diameter_min": sum_kilometers_estimated_diameter_min
                }
                req_worker.output_req.cache = json.dumps(raw_cache_dict)
                print(req_worker.output_req.cache)
        else:
            print("__ERROR__")
            req_worker.output_req.error = Error()
            req_worker.output_req.error.code = int(ans.status_code)
            req_worker.output_req.error.message = json.dumps(ans.json())
            req_worker.output_req.cache = "BROKEN_DATA"
            req_worker.output_req.response = json.dumps(ans.json())
            #print(req_worker.output_req.error.code, req_worker.output_req.error.message)

        req.post(req_worker.input_req.callback_url, data=req_worker.serialise_json())

    except Exception as ex:
        print(ex)


def get_api_key(url):
    for i in url.split('&'):
        match = re.findall("api_key=(.{1,800})", i)
        if len(match) > 0:
            return match[0]
    return "-"

def get_call_time(log_time_format='%Y-%m-%dT%H:%M:%S.%f'):
    return datetime.today().strftime(log_time_format)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)